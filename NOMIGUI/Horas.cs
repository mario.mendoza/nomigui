﻿using BLL;
using Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NOMIGUI
{
    public partial class Horas : Form
    {
        EmpleadoService empleadoService = new EmpleadoService(ConfigConnection.connectionString);
        RespuestaConsultarIdentificacion resp = new RespuestaConsultarIdentificacion();
        
        public Horas()
        {
            InitializeComponent();
            
            CenterToScreen();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            resp = empleadoService.GuardarLlegada(resp.empleado);
            MessageBox.Show(resp.Mensaje, "Reporte de estado", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void button3_Click(object sender, EventArgs e)
        {           
            string id = textID.Text.Trim();

            if (textID.Text.Equals(""))
            {
                MessageBox.Show("Digite identificación");
            }
            else
            {
                if (resp.Error is false)
                {
                    MessageBox.Show(resp.Mensaje, "Prueba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    string identificacion = textID.Text.Trim();
                    resp = empleadoService.ConsultarIdentificacion(identificacion);
                    MapearParcial(resp.empleado);
                }
                else
                {
                    MessageBox.Show(resp.Mensaje, "Prueba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                   
                }

                textID.Text = "";
            }
        }

        int indice;
        private void MapearParcial(Empleado empleado)
        {
            indice = TblEmpleados.Rows.Add();

            TblEmpleados.Rows[indice].Cells[0].Value = empleado.Cedula;
            TblEmpleados.Rows[indice].Cells[1].Value = empleado.PrimerNombre;
            TblEmpleados.Rows[indice].Cells[2].Value = empleado.PrimerApellido;

        }

        private void TblEmpleados_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            indice = e.RowIndex;
            if (indice != -1)
            {
                textID.Text = TblEmpleados.Rows[indice].Cells[0].Value.ToString();
            }
        }

        private void TblEmpleados_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            HistorialLlegadas histo = new HistorialLlegadas();
            histo.Show();
        }
    }
}
