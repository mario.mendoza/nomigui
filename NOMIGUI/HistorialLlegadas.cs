﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NOMIGUI
{
    public partial class HistorialLlegadas : Form
    {
        HorasService horasService = new HorasService(ConfigConnection.connectionString);
        List<Entity.Horas> horasllegadas;
        public HistorialLlegadas()
        {
            InitializeComponent();
            CenterToScreen();
            horasllegadas = new List<Entity.Horas>();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ConsultaHorasRespuesta respuesta = new ConsultaHorasRespuesta();
            respuesta = horasService.Consultar();
            if (respuesta.Error is false)
            {
                TablaLlegadas.DataSource = null;
                horasllegadas = null;

                TablaLlegadas.DataSource = null;
                horasllegadas = respuesta.Horas;
                TablaLlegadas.DataSource = respuesta.Horas;
                MessageBox.Show(respuesta.Mensaje, "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(respuesta.Mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
