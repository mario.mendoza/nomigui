﻿using DAL;
using Entity;
using System;
using System.Collections.Generic;

namespace BLL
{
    public class RegistroPagosService
    {
        private readonly ConecctionManager conexion;
        private readonly RegistroPagosRepository registroPagosRepository;

        public RegistroPagosService(string connectionString)
        {
            conexion = new ConecctionManager(connectionString);
            registroPagosRepository = new RegistroPagosRepository(conexion);
        }

        public string Guardar(RegistroLiquidacion registroLiquidacion)
        {

            try
            {
                conexion.Open();

                registroPagosRepository.Guardar(registroLiquidacion);

                conexion.Close();

                return $"se guardaron los datos correctamente ";
            }
            catch (System.Exception eg)
            {

                return "Error de Datos " + eg.Message;
            }
            finally
            {
                conexion.Close();
            }
        }
    }
}
