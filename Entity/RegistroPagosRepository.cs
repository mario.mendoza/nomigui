﻿using Entity;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;

namespace DAL
{
    public class RegistroPagosRepository
    {
        private readonly OracleConnection _connection;
        public RegistroPagosRepository(ConecctionManager connection)
        {
            _connection = connection._conexion;
        }

        public int Guardar(RegistroLiquidacion registroLiquidacion)
        {
            using (var Comando = _connection.CreateCommand())
            {
                Comando.CommandText = "INSERT INTO registropagos (codpago, cedula, fecha, subtotal, descuento, bonificacion, total) VALUES" +
                    "(PAGOS.NEXTVAL, :cedula, :fecha, :subtotal, :decuento, :bonificacion, :total)";
                Comando.Parameters.Add("cedula", OracleDbType.Varchar2).Value = registroLiquidacion.Empleado.Cedula;
                Comando.Parameters.Add("fecha", OracleDbType.Varchar2).Value = registroLiquidacion.FechaPago;
                Comando.Parameters.Add("subtotal", OracleDbType.Double).Value = registroLiquidacion.Salario;
                Comando.Parameters.Add("descuento", OracleDbType.Double).Value = registroLiquidacion.Descuento;
                Comando.Parameters.Add("bonificacion", OracleDbType.Double).Value = registroLiquidacion.Bonificacion;
                Comando.Parameters.Add("total", OracleDbType.Double).Value = registroLiquidacion.TotalPago;

                var filas = Comando.ExecuteNonQuery();
                return filas;
            }
        }

    }
}
